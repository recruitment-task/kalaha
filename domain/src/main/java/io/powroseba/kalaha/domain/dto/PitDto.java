package io.powroseba.kalaha.domain.dto;

import io.powroseba.kalaha.domain.players.Player;

public final class PitDto {

    public final int stonesAmount;
    public final Player owner;
    public final boolean home;

    public PitDto(int stonesAmount, Player owner, boolean home) {
        this.stonesAmount = stonesAmount;
        this.owner = owner;
        this.home = home;
    }
}
