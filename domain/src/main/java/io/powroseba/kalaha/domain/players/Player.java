package io.powroseba.kalaha.domain.players;

import java.util.Objects;
import java.util.UUID;

public abstract class Player {

    private final UUID id;

    protected Player(UUID id) {
        this.id = id;
    }

    public static FirstPlayer first(UUID id) {
        if (id == null) {
            throw new IllegalArgumentException("Passed id is null");
        } else {
            return new FirstPlayer(id);
        }
    }

    public static SecondPlayer second(UUID id) {
        if (id == null) {
            throw new IllegalArgumentException("Passed id is null");
        } else {
            return new SecondPlayer(id);
        }
    }

    public UUID getId() {
        return id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null) return false;
        if (!(o instanceof Player)) return false;
        Player player = (Player) o;
        return Objects.equals(id, player.id);
    }

    @Override
    public int hashCode() {
        return 0;
    }
}
