package io.powroseba.kalaha.domain;

import io.powroseba.kalaha.domain.dto.BoardDto;
import io.powroseba.kalaha.domain.dto.GameDto;
import io.powroseba.kalaha.domain.players.FirstPlayer;
import io.powroseba.kalaha.domain.players.Player;
import io.powroseba.kalaha.domain.players.SecondPlayer;

import java.util.UUID;

public final class Game {

    private final UUID gameId;
    private final FirstPlayer firstPlayer;
    private final SecondPlayer secondPlayer;
    private final Board board;
    private Player turn;

    Game(UUID gameId, FirstPlayer firstPlayer, SecondPlayer secondPlayer, Player turn, Board board) {
        this.gameId = gameId;
        this.firstPlayer = firstPlayer;
        this.secondPlayer = secondPlayer;
        this.board = board;
        this.turn = turn;
    }

    public static  Game start(FirstPlayer firstPlayer, SecondPlayer secondPlayer) {
        if (firstPlayer.getId().equals(secondPlayer.getId())) {
            throw new IllegalArgumentException("Game cannot be created for players with the same identifiers");
        }
        return new Game(UUID.randomUUID(), firstPlayer, secondPlayer, firstPlayer, Board.init(firstPlayer, secondPlayer));
    }

    public GameDto getData() {
        return new GameDto(
                this.gameId,
                this.turn,
                new BoardDto(firstPlayer, secondPlayer, board.getPits())
        );
    }

    public void makeMove(Move move) {
        Pit pit = board.get(move.position);
        validateMovePossibility(move, pit);
        Pit destinationPit = board.nextPit(pit);
        while(!pit.isEmpty()) {
            if (destinationPit.isHomeOf(rivalOf(move.player))) {
                destinationPit = board.nextPit(destinationPit);
                continue;
            }
            pit.moveStoneTo(destinationPit);
            destinationPit = !pit.isEmpty() ? board.nextPit(destinationPit) : destinationPit;
        }
        if (isMoveOfTakingRivalStonesFromOppositePit(move, destinationPit)) {
            final Pit home = board.getHome(move.player);
            destinationPit.moveStonesTo(home);
            board.getOpposite(destinationPit).ifPresent(oppositePit -> oppositePit.moveStonesTo(home));
        }
        if (!destinationPit.isHomeOf(move.player)) {
            changeTurn();
        }
    }

    Player getWinner() {
        if (board.anySimplePitContainStones()) { throw new IllegalStateException("Game not finished already"); }
        return board.getPlayerWithHigherStonesInHome();
    }

    private void validateMovePossibility(Move move, Pit pit) {
        if (!board.anySimplePitContainStones()) {
            throw new IllegalStateException("Game is finished");
        }
        if (!turn.equals(move.player)) {
            throw new IllegalStateException("It is the turn of other player");
        }
        if (!pit.isNotEmptySimplePitOwnedBy(move.player)) {
            throw new IllegalStateException("This pit cannot be picked");
        }
    }

    private void changeTurn() {
        this.turn = this.turn.equals(firstPlayer) ? this.secondPlayer : this.firstPlayer;
    }

    private Player rivalOf(Player player) {
        return player.equals(firstPlayer) ? secondPlayer : firstPlayer;
    }

    private boolean isMoveOfTakingRivalStonesFromOppositePit(Move move, Pit destinationPit) {
        return destinationPit.isSimplePitWithOneStoneOwnedBy(move.player)
                && board.getOpposite(destinationPit).map(opposite -> !opposite.isEmpty()).orElse(false);
    }
}
