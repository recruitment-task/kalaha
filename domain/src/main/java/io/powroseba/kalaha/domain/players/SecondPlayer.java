package io.powroseba.kalaha.domain.players;

import java.util.UUID;

public final class SecondPlayer extends Player {

    SecondPlayer(UUID id) {
        super(id);
    }
}
