package io.powroseba.kalaha.domain.dto;

import io.powroseba.kalaha.domain.Position;

import java.util.UUID;

public final class MoveDto {
    public final UUID gameId;
    public final Position position;

    public MoveDto(UUID gameId, Position position) {
        this.gameId = gameId;
        this.position = position;
    }
}
