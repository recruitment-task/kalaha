package io.powroseba.kalaha.domain.dto;

import io.powroseba.kalaha.domain.Position;
import io.powroseba.kalaha.domain.dto.BoardDto;
import io.powroseba.kalaha.domain.dto.PitDto;
import io.powroseba.kalaha.domain.players.FirstPlayer;
import io.powroseba.kalaha.domain.players.Player;
import io.powroseba.kalaha.domain.players.SecondPlayer;

import java.util.Collections;
import java.util.Map;
import java.util.UUID;

public class GameDto {

    public final UUID gameId;
    public final Player turn;
    public final BoardDto boardDto;

    public GameDto(UUID gameId, Player turn, BoardDto boardDto) {
        this.gameId = gameId;
        this.turn = turn;
        this.boardDto = boardDto;
    }

    public Map<Position, PitDto> getPits() {
        return boardDto.pits;
    }

    public FirstPlayer getFirstPlayer() {
        return boardDto.firstPlayer;
    }

    public SecondPlayer getSecondPlayer() {
        return boardDto.secondPlayer;
    }

}
