package io.powroseba.kalaha.domain;

import java.util.Optional;

public enum Position {
    P1(1),
    P2(2),
    P3(3),
    P4(4),
    P5(5),
    P6(6),
    P7(7),
    P8(8),
    P9(9),
    P10(10),
    P11(11),
    P12(12),
    P13(13),
    P14(14);

    private final int index;

    Position(int index) {
        this.index = index;
    }

    int getIndex() {
        return index;
    }

    static Optional<Position> of(int index) {
        try {
            return Optional.of(valueOf("P" + index));
        } catch (IllegalArgumentException ex) {
            return Optional.empty();
        }
    }

}
