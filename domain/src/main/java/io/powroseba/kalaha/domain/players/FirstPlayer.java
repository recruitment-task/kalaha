package io.powroseba.kalaha.domain.players;

import java.util.UUID;

public final class FirstPlayer extends Player {

    FirstPlayer(UUID id) {
        super(id);
    }
}
