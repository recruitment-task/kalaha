package io.powroseba.kalaha.domain;

import io.powroseba.kalaha.domain.dto.PitDto;
import io.powroseba.kalaha.domain.players.Player;

import java.util.Objects;
import java.util.UUID;

final class Pit {

    private static final int INITIAL_SIMPLE_PIT_STONE_AMOUNT = 4;
    private static final int INITIAL_HOME_PIT_STONE_AMOUNT = 0;

    private final UUID pitUniqueId = UUID.randomUUID();
    private int stonesAmount;
    private final Player owner;
    private final boolean home;

    Pit(int stonesAmount, boolean isHome, Player owner) {
        this.stonesAmount = stonesAmount;
        this.owner = owner;
        this.home = isHome;
    }

    static Pit home(Player player) {
        return new Pit(INITIAL_HOME_PIT_STONE_AMOUNT, true, player);
    }

    static Pit simple(Player player) {
        return new Pit(INITIAL_SIMPLE_PIT_STONE_AMOUNT, false, player);
    }

    boolean isEmpty() {
        return this.stonesAmount == 0;
    }

    boolean isOwnedBy(Player player) {
        return owner.equals(player);
    }

    boolean isHomeOf(Player player) {
        return home && isOwnedBy(player);
    }

    void moveStoneTo(final Pit destinationPit) {
        if (!this.isEmpty()) {
            this.takeStone();
            destinationPit.putStone();
        }
    }

    void moveStonesTo(final Pit destinationPit) {
        while(!this.isEmpty()) {
            this.moveStoneTo(destinationPit);
        }
    }

    boolean isSimplePitWithOneStoneOwnedBy(Player player) {
        return stonesAmount == 1 && this.isOwnedBy(player) && !this.isHomeOf(player);
    }

    boolean isNotEmptySimplePitOwnedBy(Player player) {
        return this.isOwnedBy(player) && !this.isEmpty() && !this.isHomeOf(player);
    }

    boolean isEmptySimplePitOwnedBy(Player player) {
        return this.isOwnedBy(player) && this.isEmpty() && !this.isHomeOf(player);
    }

    boolean haveMoreStonesThan(Pit pit) {
        return this.stonesAmount > pit.stonesAmount;
    }

    PitDto toDto() {
        return new PitDto(stonesAmount, owner, home);
    }

    UUID uniqueId() { return this.pitUniqueId; }

    private void putStone() {
        this.stonesAmount += 1;
    }

    private void takeStone() {
        this.stonesAmount -= 1;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pit pit = (Pit) o;
        return stonesAmount == pit.stonesAmount && home == pit.home && Objects.equals(owner, pit.owner);
    }

    @Override
    public int hashCode() {
        return Objects.hash(stonesAmount, owner, home);
    }
}
