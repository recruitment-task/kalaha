package io.powroseba.kalaha.domain;

import io.powroseba.kalaha.domain.dto.BoardDto;
import io.powroseba.kalaha.domain.dto.PitDto;
import io.powroseba.kalaha.domain.players.FirstPlayer;
import io.powroseba.kalaha.domain.players.Player;
import io.powroseba.kalaha.domain.players.SecondPlayer;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

class Board {

    private static final int LAST_POSITION_INDEX = 14;
    private static final int FIRST_POSITION_INDEX = 1;

    private final FirstPlayer firstPlayer;
    private final SecondPlayer secondPlayer;
    private final Map<Position, Pit> pits = new HashMap<>();

    Board(FirstPlayer firstPlayer, SecondPlayer secondPlayer, Map<Position, Pit> pits) {
        this.firstPlayer = firstPlayer;
        this.secondPlayer = secondPlayer;
        this.pits.putAll(pits);
    }

    static Board init(FirstPlayer firstPlayer, SecondPlayer secondPlayer) {
        Board board = new Board(firstPlayer, secondPlayer, new HashMap<>());
        board.putFirstPlayerPits(firstPlayer);
        board.putSecondPlayerPits(secondPlayer);
        return board;
    }

    Pit get(final Position position) {
        return pits.get(position);
    }

    Pit nextPit(Pit pit) {
        return getPitPosition(pit)
                .map(Position::getIndex)
                .flatMap(this::getNextPitIndex)
                .map(pits::get)
                .orElse(null);
    }

    Pit getHome(Player player) {
        if (firstPlayer.equals(player)) {
            return get(Position.P7);
        }
        if (secondPlayer.equals(player)) {
            return get(Position.P14);
        }
        throw new IllegalArgumentException("Unknown player id : " + player.getId());
    }

    Optional<Pit> getOpposite(Pit pit) {
        return getPitPosition(pit)
                .filter(this::hasOppositePosition)
                .map(Position::getIndex)
                .flatMap(index -> Position.of(LAST_POSITION_INDEX - index))
                .map(this::get);
    }

    Map<Position, PitDto> getPits() {
        return pits.entrySet().stream().collect(Collectors.toUnmodifiableMap(Map.Entry::getKey, entry -> entry.getValue().toDto()));
    }

    boolean anySimplePitContainStones() {
        return anySimplePitWithStonesForPlayer(firstPlayer) && anySimplePitWithStonesForPlayer(secondPlayer);
    }

    Player getPlayerWithHigherStonesInHome() {
        return this.getHome(firstPlayer).haveMoreStonesThan(this.getHome(secondPlayer)) ? firstPlayer : secondPlayer;
    }

    private boolean anySimplePitWithStonesForPlayer(Player player) {
        return pits
                .values()
                .stream()
                .filter(pit -> pit.isEmptySimplePitOwnedBy(player))
                .count() < 6;
    }

    private Optional<Position> getNextPitIndex(Integer index) {
        return index == LAST_POSITION_INDEX ? Position.of(FIRST_POSITION_INDEX) : Position.of(index + 1);
    }

    private Optional<Position> getPitPosition(Pit pit) {
        return pits.entrySet()
                .stream()
                .filter(entry -> pit.uniqueId().equals(entry.getValue().uniqueId()))
                .map(Map.Entry::getKey)
                .findFirst();
    }

    private boolean hasOppositePosition(Position position) {
        return !Position.P7.equals(position) && !Position.P14.equals(position);
    }

    private void putFirstPlayerPits(FirstPlayer firstPlayer) {
        this.pits.put(Position.P1, Pit.simple(firstPlayer));
        this.pits.put(Position.P2, Pit.simple(firstPlayer));
        this.pits.put(Position.P3, Pit.simple(firstPlayer));
        this.pits.put(Position.P4, Pit.simple(firstPlayer));
        this.pits.put(Position.P5, Pit.simple(firstPlayer));
        this.pits.put(Position.P6, Pit.simple(firstPlayer));
        this.pits.put(Position.P7, Pit.home(firstPlayer));
    }

    private void putSecondPlayerPits(SecondPlayer secondPlayer) {
        this.pits.put(Position.P8, Pit.simple(secondPlayer));
        this.pits.put(Position.P9, Pit.simple(secondPlayer));
        this.pits.put(Position.P10, Pit.simple(secondPlayer));
        this.pits.put(Position.P11, Pit.simple(secondPlayer));
        this.pits.put(Position.P12, Pit.simple(secondPlayer));
        this.pits.put(Position.P13, Pit.simple(secondPlayer));
        this.pits.put(Position.P14, Pit.home(secondPlayer));
    }
}
