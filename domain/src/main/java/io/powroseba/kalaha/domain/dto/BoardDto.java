package io.powroseba.kalaha.domain.dto;

import io.powroseba.kalaha.domain.Position;
import io.powroseba.kalaha.domain.players.FirstPlayer;
import io.powroseba.kalaha.domain.players.SecondPlayer;

import java.util.Map;

public class BoardDto {

    public final FirstPlayer firstPlayer;
    public final SecondPlayer secondPlayer;
    public final Map<Position, PitDto> pits;

    public BoardDto(FirstPlayer firstPlayer, SecondPlayer secondPlayer, Map<Position, PitDto> pits) {
        this.firstPlayer = firstPlayer;
        this.secondPlayer = secondPlayer;
        this.pits = pits;
    }
}
