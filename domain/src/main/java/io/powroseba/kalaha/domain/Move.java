package io.powroseba.kalaha.domain;

import io.powroseba.kalaha.domain.players.Player;

public final class Move {
    public final Position position;
    public  final Player player;

    public Move(Position position, Player player) {
        this.position = position;
        this.player = player;
    }

}
