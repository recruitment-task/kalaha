package io.powroseba.kalaha.domain

import io.powroseba.kalaha.domain.dto.BoardDto
import io.powroseba.kalaha.domain.dto.PitDto
import io.powroseba.kalaha.domain.players.FirstPlayer
import io.powroseba.kalaha.domain.players.Player
import io.powroseba.kalaha.domain.players.SecondPlayer

import static io.powroseba.kalaha.domain.Position.*

final class BoardDataFactory {

    private final FirstPlayer firstPlayer
    private final SecondPlayer secondPlayer

    BoardDataFactory(FirstPlayer firstPlayer, SecondPlayer secondPlayer) {
        this.firstPlayer = firstPlayer
        this.secondPlayer = secondPlayer
    }

    BoardDto initialData() {
        return new BoardDto(firstPlayer, secondPlayer, initialPits())
    }

    BoardDto finishedGameFor(Player winner) {
        Map<Position, PitDto> pits = initialPits()
        if (winner == firstPlayer) {
            Arrays.asList(P1, P2, P3, P4, P5, P6)
            .forEach({ position -> pits.put(position, new PitDto(0, winner, false)) })
            pits.put(P7, new PitDto(20, winner, true))
        }
        if (winner == secondPlayer) {
            Arrays.asList(P8, P9, P10, P11, P12, P13)
             .forEach({ position -> pits.put(position, new PitDto(0, winner, false)) })
            pits.put(P14, new PitDto(20, winner, true))
        }
        return new BoardDto(firstPlayer, secondPlayer, pits)
    }

    private HashMap<Position, PitDto> initialPits() {
        Map<Position, PitDto> pits = new HashMap<>()
        pits.put(P1, Pit.simple(firstPlayer).toDto())
        pits.put(P2, Pit.simple(firstPlayer).toDto())
        pits.put(P3, Pit.simple(firstPlayer).toDto())
        pits.put(P4, Pit.simple(firstPlayer).toDto())
        pits.put(P5, Pit.simple(firstPlayer).toDto())
        pits.put(P6, Pit.simple(firstPlayer).toDto())
        pits.put(P7, Pit.home(firstPlayer).toDto())
        pits.put(P8, Pit.simple(secondPlayer).toDto())
        pits.put(P9, Pit.simple(secondPlayer).toDto())
        pits.put(P10, Pit.simple(secondPlayer).toDto())
        pits.put(P11, Pit.simple(secondPlayer).toDto())
        pits.put(P12, Pit.simple(secondPlayer).toDto())
        pits.put(P13, Pit.simple(secondPlayer).toDto())
        pits.put(P14, Pit.home(secondPlayer).toDto())
        pits
    }
}
