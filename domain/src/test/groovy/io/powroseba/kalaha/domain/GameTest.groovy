package io.powroseba.kalaha.domain

import io.powroseba.kalaha.domain.dto.PitDto
import io.powroseba.kalaha.domain.players.FirstPlayer
import io.powroseba.kalaha.domain.players.Player
import io.powroseba.kalaha.domain.players.SecondPlayer
import spock.lang.Specification
import spock.lang.Unroll

import static io.powroseba.kalaha.domain.GameFixture.newGame

class GameTest extends Specification {

    private static final FirstPlayer FIRST_PLAYER = Player.first(UUID.randomUUID())
    private static final SecondPlayer SECOND_PLAYER = Player.second(UUID.randomUUID())

    static {
        Pit.metaClass.isSimpleWith4StonesOwnedBy = {
            Player player -> delegate == new Pit(4, false, player)
        }
        Pit.metaClass.isEmptyHomeOwnerBy = {
            Player player -> delegate == new Pit(0, true, player)
        }
        PitDto.metaClass.toPit = { ->
            return new Pit(delegate.stonesAmount, delegate.home, delegate.owner)
        }
    }

    def 'should not create game with players with the same identifiers'() {
        given:
            final UUID id = UUID.randomUUID()

        when:
            Game.start(Player.first(id), Player.second(id))

        then:
            def exception = thrown(IllegalArgumentException)
            exception.message == "Game cannot be created for players with the same identifiers"
    }

    def 'should throw IllegalStateException when player who not turn is trying to make move'() {
        given:
            Game game = newGame(SECOND_PLAYER, FIRST_PLAYER)
                    .withTurnForPlayer(FIRST_PLAYER)
                    .get()

        when:
            game.makeMove(new Move(Position.P1, SECOND_PLAYER))

        then:
            def exception = thrown(IllegalStateException)
            exception.message == "It is the turn of other player"
    }

    def 'should throw IllegalStateException when game is finished'() {
        given:
            Game game = newGame(FIRST_PLAYER, SECOND_PLAYER)
                    .finished()

        when:
            game.makeMove(new Move(Position.P1, SECOND_PLAYER))

        then:
            def exception = thrown(IllegalStateException)
            exception.message == "Game is finished"
    }

    def 'should throw IllegalStateException when indicate pit is not owned by player who is making move'() {
        given:
            Game game = newGame(FIRST_PLAYER, SECOND_PLAYER)
                    .withPitForPlayer(FIRST_PLAYER, Position.P8)
                    .withTurnForPlayer(FIRST_PLAYER)
                    .get()

        when:
            game.makeMove(new Move(Position.P1, FIRST_PLAYER))

        then:
            def exception = thrown(IllegalStateException)
            exception.message == "This pit cannot be picked"
    }

    def 'should throw IllegalStateException when indicate pit is empty'() {
        given:
            Game game = newGame(FIRST_PLAYER, SECOND_PLAYER)
                    .withPitForPlayer(FIRST_PLAYER, Position.P3)
                    .withTurnForPlayer(FIRST_PLAYER)
                    .get()

        and:
            game.makeMove(new Move(Position.P3, FIRST_PLAYER))

        when:
            game.makeMove(new Move(Position.P3, FIRST_PLAYER))

        then:
            def exception = thrown(IllegalStateException)
            exception.message == "This pit cannot be picked"
    }

    def 'should move 4 stones from P2 to next pits by first player'() {
        given:
            Game game = newGame(FIRST_PLAYER, SECOND_PLAYER)
                    .withPitForPlayer(FIRST_PLAYER, Position.P2)
                    .withTurnForPlayer(FIRST_PLAYER)
                    .get()

        when:
            game.makeMove(new Move(Position.P2, FIRST_PLAYER))

        then:
            game.getData().getPits().get(Position.P2).stonesAmount == 0
            game.getData().getPits().get(Position.P3).stonesAmount == 5
            game.getData().getPits().get(Position.P4).stonesAmount == 5
            game.getData().getPits().get(Position.P5).stonesAmount == 5
            game.getData().getPits().get(Position.P6).stonesAmount == 5
            game.getData().getPits().get(Position.P7).stonesAmount == 0

            game.getData().turn != FIRST_PLAYER
            noExceptionThrown()
    }

    def 'should not change turn when last stone land into own home'() {
        given:
            Game game = newGame(FIRST_PLAYER, SECOND_PLAYER)
                    .withPitForPlayer(FIRST_PLAYER, Position.P3)
                    .withTurnForPlayer(FIRST_PLAYER)
                    .get()

        when:
            game.makeMove(new Move(Position.P3, FIRST_PLAYER))

        then:
            game.getData().getPits().get(Position.P3).stonesAmount == 0
            game.getData().getPits().get(Position.P4).stonesAmount == 5
            game.getData().getPits().get(Position.P5).stonesAmount == 5
            game.getData().getPits().get(Position.P6).stonesAmount == 5
            game.getData().getPits().get(Position.P7).stonesAmount == 1

            game.getData().turn == FIRST_PLAYER
            noExceptionThrown()
    }

    def 'should move stones from own pit and opposite pit to home when destination pit was empty before move'() {
        given:
            Game game = newGame(FIRST_PLAYER, SECOND_PLAYER)
                    .withStones_4InP8_0InP12_11InP2_4InP14WhereP14_P8_P12IsOwnerBy(FIRST_PLAYER)
            Player turn = game.getData().turn

        when:
            game.makeMove(new Move(Position.P8, FIRST_PLAYER))

        then:
            game.getData().getPits().get(Position.P2).stonesAmount == 0
            game.getData().getPits().get(Position.P8).stonesAmount == 0
            game.getData().getPits().get(Position.P12).stonesAmount == 0
            game.getData().getPits().get(Position.P14).stonesAmount == 16

            game.getData().turn != turn
            noExceptionThrown()
    }

    def 'should not move stone from destination pit and opposite pit when destination pit was empty before move and opposite pit is empty'() {
        given:
            Game game = newGame(FIRST_PLAYER, SECOND_PLAYER)
                    .withStones_2InP11_0InP13_26InP14_0InP1WhereP11_P13_P14IsOwnedBy(FIRST_PLAYER)
            Player turn = game.getData().turn

        when:
            game.makeMove(new Move(Position.P11, FIRST_PLAYER))

        then:
            game.getData().getPits().get(Position.P11).stonesAmount == 0
            game.getData().getPits().get(Position.P13).stonesAmount == 1
            game.getData().getPits().get(Position.P1).stonesAmount == 0
            game.getData().getPits().get(Position.P14).stonesAmount == 26

            game.getData().turn != turn
            noExceptionThrown()
    }

    def 'should not be ably to move if indicated position is home position = #homePitPosition'() {
        given:
            Game game = newGame(FIRST_PLAYER, SECOND_PLAYER)
                    .withPitForPlayer(FIRST_PLAYER, Position.P7)
                    .withTurnForPlayer(FIRST_PLAYER)
                    .get()

        when:
            game.makeMove(new Move(Position.P7, FIRST_PLAYER))

        then:
            def firstPlayerException = thrown(IllegalStateException)
            firstPlayerException.message == "This pit cannot be picked"

        and: 'make one move with first player to change turn for second player'
            game.makeMove(new Move(Position.P1, FIRST_PLAYER))

        when:
            game.makeMove(new Move(Position.P14, SECOND_PLAYER))

        then:
            def secondPlayerException = thrown(IllegalStateException)
            secondPlayerException.message == "This pit cannot be picked"

    }

    def 'should create game with initial board state'() {
        when:
            Game game = Game.start(FIRST_PLAYER, SECOND_PLAYER)

        then:
            with(game.getData()) {
                Arrays.asList(firstPlayer, secondPlayer).contains(turn)
                getSecondPlayer() == secondPlayer
                getFirstPlayer() == firstPlayer
                gameId != null
                getPits().get(Position.P1).toPit().isSimpleWith4StonesOwnedBy(firstPlayer)
                getPits().get(Position.P2).toPit().isSimpleWith4StonesOwnedBy(firstPlayer)
                getPits().get(Position.P3).toPit().isSimpleWith4StonesOwnedBy(firstPlayer)
                getPits().get(Position.P4).toPit().isSimpleWith4StonesOwnedBy(firstPlayer)
                getPits().get(Position.P5).toPit().isSimpleWith4StonesOwnedBy(firstPlayer)
                getPits().get(Position.P6).toPit().isSimpleWith4StonesOwnedBy(firstPlayer)
                getPits().get(Position.P7).toPit().isEmptyHomeOwnerBy(firstPlayer)
                getPits().get(Position.P8).toPit().isSimpleWith4StonesOwnedBy(secondPlayer)
                getPits().get(Position.P9).toPit().isSimpleWith4StonesOwnedBy(secondPlayer)
                getPits().get(Position.P10).toPit().isSimpleWith4StonesOwnedBy(secondPlayer)
                getPits().get(Position.P11).toPit().isSimpleWith4StonesOwnedBy(secondPlayer)
                getPits().get(Position.P12).toPit().isSimpleWith4StonesOwnedBy(secondPlayer)
                getPits().get(Position.P13).toPit().isSimpleWith4StonesOwnedBy(secondPlayer)
                getPits().get(Position.P14).toPit().isEmptyHomeOwnerBy(secondPlayer)
            }
            noExceptionThrown()
    }

    def 'should throw IllegalStateException when game is not finished already'() {
        given:
            Game game = Game.start(FIRST_PLAYER, SECOND_PLAYER)

        when:
            game.getWinner()

        then:
            def exception = thrown(IllegalStateException)
            exception.message == "Game not finished already"
    }

    @Unroll
    def 'should return #winner as a payer who win the game'() {
        given:
            Game game = newGame(FIRST_PLAYER, SECOND_PLAYER)
                    .finishedBy(winner)

        when:
            Player playerWhoWin = game.getWinner()

        then:
            playerWhoWin == winner
            noExceptionThrown()

        where:
            winner << [FIRST_PLAYER, SECOND_PLAYER]
    }
}
