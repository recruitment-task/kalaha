package io.powroseba.kalaha.domain

import io.powroseba.kalaha.domain.dto.BoardDto
import io.powroseba.kalaha.domain.dto.PitDto
import io.powroseba.kalaha.domain.players.FirstPlayer
import io.powroseba.kalaha.domain.players.Player
import io.powroseba.kalaha.domain.players.SecondPlayer
import spock.lang.Specification
import spock.lang.Unroll

import java.util.stream.Collectors
import java.util.stream.IntStream
import java.util.stream.Stream

class BoardTest extends Specification {

    /**
     * Board pit positions
     *      P13 P12 P11 P10 P9  P8
     * P14                          P7
     *      P1  P2  P3  P4  P5  P6
     */

    private static final FirstPlayer FIRST_PLAYER = Player.first(UUID.randomUUID())
    private static final SecondPlayer SECOND_PLAYER = Player.second(UUID.randomUUID())

    private Board board

    def setup() {
        this.board = Board.init(FIRST_PLAYER, SECOND_PLAYER)
    }

    def 'should return Pit from position P1 which is not null'() {
        when:
            Pit pit = board.get(Position.P1)

        then:
            pit != null
            noExceptionThrown()
    }

    def 'should return Pit from position P1 which is owned by first player'() {
        when:
            Pit pit = board.get(Position.P1)

        then:
            pit.isOwnedBy(FIRST_PLAYER)
            noExceptionThrown()
    }

    def 'should return Pit from position P8 which is owned by second player'() {
        when:
            Pit pit = board.get(Position.P8)

        then:
            pit.isOwnedBy(SECOND_PLAYER)
            noExceptionThrown()
    }

    def 'should always return the same pit for the same position'() {
        given:
            Pit pit = board.get(Position.P1)

        when:
            Pit result = board.get(Position.P1)

        then:
            result == pit
            noExceptionThrown()
    }

    @Unroll
    def 'should return home pit of player #player from position #pitPosition'() {
        when:
            Pit pit = board.get(pitPosition)

        then:
            pit.isHomeOf(player)
            noExceptionThrown()

        where:
            player       || pitPosition
            FIRST_PLAYER || Position.P7
            SECOND_PLAYER || Position.P14
    }

    @Unroll
    def 'should return pits from position #startPosition to #endPosition which are simple pits of player #player'() {
        when:
            List<Pit> pits = IntStream.range(startPosition.getIndex(), endPosition.getIndex() + 1)
             .mapToObj({ int index -> Position.of(index).get() })
             .map({ Position position -> board.get(position) })
             .collect(Collectors.toList())

        then:
            pits.stream().allMatch({ it == new Pit(4, false, player)})

        where:
            startPosition | endPosition  || player
            Position.P1   | Position.P6  || FIRST_PLAYER
            Position.P8   | Position.P13 || SECOND_PLAYER
    }

    def 'should return pit from P2 position while getting next pit from pit in position P1'() {
        given:
            Pit pitInP1Position = board.get(Position.P1)
            Pit pitInP2Position = board.get(Position.P2)

        when:
            Pit result = board.nextPit(pitInP1Position)

        then:
            result == pitInP2Position
            noExceptionThrown()
    }

    def 'should return pit from P1 position while getting next pit from pit in position P14'() {
        given:
            Pit pitInP14Position = board.get(Position.P14)
            Pit pitInP1Position = board.get(Position.P1)

        when:
            Pit result = board.nextPit(pitInP14Position)

        then:
            result == pitInP1Position
            noExceptionThrown()
    }

    @Unroll
    def 'next pit from pit in position #previousPosition should be equal with pit in position #nextPosition'() {
        given:
            Pit previousPit = board.get(previousPosition)
            Pit nextPit = board.get(nextPosition)

        when:
            Pit result = board.nextPit(previousPit)

        then:
            result == nextPit
            noExceptionThrown()

        where:
            previousPosition || nextPosition
            Position.P1      || Position.P2
            Position.P2      || Position.P3
            Position.P3      || Position.P4
            Position.P4      || Position.P5
            Position.P5      || Position.P6
            Position.P6      || Position.P7
            Position.P7      || Position.P8
            Position.P8      || Position.P9
            Position.P9      || Position.P10
            Position.P10     || Position.P11
            Position.P11     || Position.P12
            Position.P12     || Position.P13
            Position.P13     || Position.P14
            Position.P14     || Position.P1

    }

    def 'should throw IllegalArgumentException while trying to get home of unknown player'() {
        given:
            Player unknownPlayer = Player.first(UUID.randomUUID())

        when:
            board.getHome(unknownPlayer)

        then:
            def exception = thrown(IllegalArgumentException)
            exception.getMessage() == "Unknown player id : " + unknownPlayer.getId()
    }

    def 'should return pit which is owned by first player'() {
        when:
            Pit pit = board.getHome(FIRST_PLAYER)

        then:
            pit.isOwnedBy(FIRST_PLAYER)
            noExceptionThrown()
    }

    def 'should return pit which is home pit of first player'() {
        when:
            Pit pit = board.getHome(FIRST_PLAYER)

        then:
            pit.isHomeOf(FIRST_PLAYER)
            noExceptionThrown()
    }

    def 'should return pit from position P7 while getting home pit of first player'() {
        given:
            Pit pitInPositionP7 = board.get(Position.P7)

        when:
            Pit homePit = board.getHome(FIRST_PLAYER)

        then:
            homePit == pitInPositionP7
            noExceptionThrown()
    }

    def 'should return pit from position P14 which is owned and home of second player'() {
        given:
            Pit pitFromP14Position = board.get(Position.P14)

        when:
            Pit homePit = board.getHome(SECOND_PLAYER)

        then:
            homePit == pitFromP14Position
            with(homePit) {
                isOwnedBy(SECOND_PLAYER)
                isHomeOf(SECOND_PLAYER)
            }
            noExceptionThrown()
    }

    @Unroll
    def 'should return empty Optional while getting opposite pit from position #position'() {
        when:
            Optional<Pit> oppositePit = board.getOpposite(board.get(position))

        then:
            oppositePit.isEmpty()
            noExceptionThrown()

        where:
            position << [Position.P7, Position.P14]
    }

    def 'should return pit from position P13 while getting opposite position from position P1'() {
        given:
            Pit pitFromP13Position = board.get(Position.P13)

        when:
            Optional<Pit> oppositePit = board.getOpposite(board.get(Position.P1))

        then:
            oppositePit.isPresent()
            oppositePit.get() == pitFromP13Position
            noExceptionThrown()
    }

    def 'should return pit from position P1 while getting opposite position from position P13'() {
        given:
            Pit pitFromP1Position = board.get(Position.P1)

        when:
            Optional<Pit> oppositePit = board.getOpposite(board.get(Position.P13))

        then:
            oppositePit.isPresent()
            oppositePit.get() == pitFromP1Position
            noExceptionThrown()
    }

    @Unroll
    def 'should return pit from position #oppositePosition while getting opposite pit from position #pitPosition'() {
        given:
            Pit sourcePit = board.get(pitPosition)
            Pit expectedPit = board.get(oppositePosition)

        when:
            Optional<Pit> oppositePit = board.getOpposite(sourcePit)

        then:
            expectedPit == oppositePit.get()
            noExceptionThrown()

        where:
            pitPosition  || oppositePosition
            Position.P1  || Position.P13
            Position.P2  || Position.P12
            Position.P3  || Position.P11
            Position.P4  || Position.P10
            Position.P5  || Position.P9
            Position.P6  || Position.P8
            Position.P13 || Position.P1
            Position.P12 || Position.P2
            Position.P11 || Position.P3
            Position.P10 || Position.P4
            Position.P9  || Position.P5
            Position.P8  || Position.P6
    }

    def 'should load pits and players from BoardDto'() {
        given:
            Map<Position, Pit> pits = new HashMap<>()
            pits.put(Position.P1, Pit.simple(FIRST_PLAYER))
            pits.put(Position.P2, Pit.simple(FIRST_PLAYER))
            pits.put(Position.P3, Pit.simple(FIRST_PLAYER))
            pits.put(Position.P4, Pit.simple(FIRST_PLAYER))
            pits.put(Position.P5, Pit.simple(FIRST_PLAYER))
            pits.put(Position.P6, Pit.simple(FIRST_PLAYER))
            pits.put(Position.P7, Pit.home(FIRST_PLAYER))
            pits.put(Position.P8, Pit.simple(SECOND_PLAYER))
            pits.put(Position.P9, Pit.simple(SECOND_PLAYER))
            pits.put(Position.P10, Pit.simple(SECOND_PLAYER))
            pits.put(Position.P11, Pit.simple(SECOND_PLAYER))
            pits.put(Position.P12, Pit.simple(SECOND_PLAYER))
            pits.put(Position.P13, Pit.simple(SECOND_PLAYER))
            pits.put(Position.P14, Pit.home(SECOND_PLAYER))

        when:
            Board board = new Board(FIRST_PLAYER, SECOND_PLAYER, pits)

        then:
            Stream.of(Position.values()).forEach({ position ->
                if (position.getIndex() < 7) {
                    assert isSimplePitWith6Stones(board.get(position), FIRST_PLAYER)
                }
                if (position.getIndex() > 7 && position.getIndex() < 14) {
                    assert isSimplePitWith6Stones(board.get(position), SECOND_PLAYER)
                }
                if (Position.P7 == position) {
                    assert isEmptyHomePit(board.get(position), FIRST_PLAYER)
                }
                if (Position.P14 == position) {
                    assert isEmptyHomePit(board.get(position), SECOND_PLAYER)
                }
            })
            noExceptionThrown()
    }

    private static boolean isSimplePitWith6Stones(Pit pit, Player player) {
        return pit == new Pit(4, false, player)
    }

    private static boolean isEmptyHomePit(Pit pit, Player player) {
        return pit == new Pit(0, true, player)
    }


}
