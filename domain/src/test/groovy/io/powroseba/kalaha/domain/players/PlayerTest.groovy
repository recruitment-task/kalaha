package io.powroseba.kalaha.domain.players

import spock.lang.Specification
import spock.lang.Unroll

class PlayerTest extends Specification {
    def 'should throw exception when first player id is null'() {
        when:
            Player.first(null)

        then:
            def exception = thrown(IllegalArgumentException)
            exception.getMessage() == "Passed id is null"
    }

    def 'should create firstPlayer without exception'() {
        when:
            Player player = Player.first(UUID.randomUUID())

        then:
            player != null
            noExceptionThrown()
    }

    def 'should throw exception when second player id is null'() {
        when:
            Player.second(null)

        then:
            def exception = thrown(IllegalArgumentException)
            exception.getMessage() == "Passed id is null"
    }

    def 'should create secondPlayer without exception'() {
        when:
            SecondPlayer player = Player.second(UUID.randomUUID())

        then:
            player != null
            noExceptionThrown()
    }

    def 'first players with the same id should be equals'() {
        given:
            final UUID id = UUID.randomUUID()
            Player playerOne = Player.first(id)
            Player playerTwo = Player.first(id)

        when:
            def result = playerOne == playerTwo

        then:
            result
            noExceptionThrown()
    }

    def 'first players with different id should not be equals'() {
        given:
            Player playerOne = Player.first(UUID.randomUUID())
            Player playerTwo = Player.first(UUID.randomUUID())

        when:
            def result = playerOne == playerTwo

        then:
            !result
            noExceptionThrown()
    }

    def 'second players with the same id should be equals'() {
        given:
            final UUID id = UUID.randomUUID()
            SecondPlayer playerOne = Player.second(id)
            SecondPlayer playerTwo = Player.second(id)

        when:
            def result = playerOne == playerTwo

        then:
            result
            noExceptionThrown()
    }

    def 'second players with different id should not be equals'() {
        given:
            SecondPlayer playerOne = Player.second(UUID.randomUUID())
            SecondPlayer playerTwo = Player.second(UUID.randomUUID())

        when:
            def result = playerOne == playerTwo

        then:
            !result
            noExceptionThrown()
    }

    @Unroll
    def 'should return id in case of #factoryMethod player'() {
        given:
            UUID playerId = UUID.randomUUID()
            Player player = Player."$factoryMethod"(playerId)

        when:
            UUID id = player.getId()

        then:
            id == playerId
            noExceptionThrown()

        where:
            factoryMethod << ['first', 'second']
    }

}
