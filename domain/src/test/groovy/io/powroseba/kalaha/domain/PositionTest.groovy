package io.powroseba.kalaha.domain

import spock.lang.Specification

class PositionTest extends Specification {

    def 'should create properly position = #expectedPosition from position number #positionIndex'() {
        when:
            def position = Position.of(positionIndex).get()

        then:
            position == expectedPosition
            expectedPosition.getIndex() == positionIndex
            noExceptionThrown()

        where:
            positionIndex || expectedPosition
            1             || Position.P1
            2             || Position.P2
            3             || Position.P3
            4             || Position.P4
            5             || Position.P5
            6             || Position.P6
            7             || Position.P7
            8             || Position.P8
            9             || Position.P9
            10            || Position.P10
            11            || Position.P11
            12            || Position.P12
            13            || Position.P13
            14            || Position.P14
    }

    def "should return Optional.empty() when invalid position index will be passed"() {
        when:
            def possiblePosition = Position.of(15)

        then:
            possiblePosition.isEmpty()
            noExceptionThrown()
    }
}
