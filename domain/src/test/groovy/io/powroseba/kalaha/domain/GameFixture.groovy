package io.powroseba.kalaha.domain

import io.powroseba.kalaha.domain.players.FirstPlayer
import io.powroseba.kalaha.domain.players.Player
import io.powroseba.kalaha.domain.players.SecondPlayer

class GameFixture {

    private GameFixture() {}

    static GameBuilder newGame(Player playerOne, Player playerTwo) {
        return new GameBuilder(Player.first(playerOne.id), Player.second(playerTwo.id))
    }

    private static class GameBuilder {

        private final FirstPlayer firstPlayer
        private final SecondPlayer secondPlayer
        private Game game

        private GameBuilder(FirstPlayer firstPlayer, SecondPlayer secondPlayer) {
            this.firstPlayer = firstPlayer
            this.secondPlayer = secondPlayer
        }

        private GameBuilder(FirstPlayer firstPlayer, SecondPlayer secondPlayer, Game game) {
            this(firstPlayer, secondPlayer)
            this.game = game
        }

        GameBuilder withPitForPlayer(Player player, Position position) {
            if (position.ordinal() <= 6) {
                def secondPlayer = this.secondPlayer == player ? Player.second(this.firstPlayer.id) : this.secondPlayer
                def firstPlayer = Player.first(player.id)
                new GameBuilder(firstPlayer, secondPlayer, Game.start(firstPlayer, secondPlayer))
            } else {
                def firstPlayer = this.firstPlayer == player ? Player.first(this.secondPlayer.id) : this.firstPlayer
                def secondPlayer = Player.second(player.id)
                return new GameBuilder(firstPlayer, secondPlayer, Game.start(firstPlayer, secondPlayer))
            }
        }

        GameBuilder withTurnForPlayer(Player player) {
            if (this.game != null) {
                try {
                    def playerOfMove = this.firstPlayer == player ? this.secondPlayer : this.firstPlayer
                    def position = playerOfMove == this.firstPlayer ? Position.P1 : Position.P8
                    this.game.makeMove(new Move(position, playerOfMove))
                    return new GameBuilder(firstPlayer, this.secondPlayer, this.game)
                } catch (IllegalStateException ex) {
                    if (ex.message == "It is the turn of other player") {
                        return this
                    }
                    throw ex
                }
            } else {
                def firstPlayer = Player.first(player.id)
                def secondPlayer = this.secondPlayer == player ? Player.second(this.firstPlayer.id) : this.secondPlayer
                return new GameBuilder(
                        firstPlayer,
                        secondPlayer,
                        Game.start(firstPlayer, secondPlayer)
                )
            }
        }

        Game withStones_4InP8_0InP12_11InP2_4InP14WhereP14_P8_P12IsOwnerBy(Player player) {
            final SecondPlayer secondPlayer = Player.second(player.id)
            final FirstPlayer firstPlayer = player == this.firstPlayer ? Player.first(this.secondPlayer.id) : this.firstPlayer
            return new GameBuilder(firstPlayer, secondPlayer)
                    .withStones_4InP8_0InP12_11InP2_4InP14()
        }

        Game withStones_2InP11_0InP13_26InP14_0InP1WhereP11_P13_P14IsOwnedBy(Player player) {
            final SecondPlayer secondPlayer = Player.second(player.id)
            final FirstPlayer firstPlayer = player == this.firstPlayer ? Player.first(this.secondPlayer.id) : this.firstPlayer
            return new GameBuilder(firstPlayer, secondPlayer)
                    .withStones_2InP11_0InP13_26InP14_0InP1()
        }

        Game finished() {
            Game game = withStones_2InP11_0InP13_26InP14_0InP1()
            game.makeMove(new Move(Position.P11, this.secondPlayer))
            game.makeMove(new Move(Position.P4, this.firstPlayer))
            game.makeMove(new Move(Position.P13, this.secondPlayer))
            game.makeMove(new Move(Position.P12, this.secondPlayer))
            game.makeMove(new Move(Position.P11, this.secondPlayer))
            game.makeMove(new Move(Position.P6, this.firstPlayer))
            game.makeMove(new Move(Position.P13, this.secondPlayer))
            game.makeMove(new Move(Position.P12, this.secondPlayer))
            game.makeMove(new Move(Position.P5, this.firstPlayer))
            game.makeMove(new Move(Position.P6, this.firstPlayer))
            return game
        }

        Game finishedBy(Player player) {
            final SecondPlayer winner = Player.second(player.id)
            final FirstPlayer loser = player == this.firstPlayer ? Player.first(this.secondPlayer.id) : this.firstPlayer
            return new GameBuilder(loser, winner).finished()
        }

        Game get() { return this.game }

        private Game withStones_4InP8_0InP12_11InP2_4InP14() {
            Game game = Game.start(this.firstPlayer, this.secondPlayer)
            game.makeMove(new Move(Position.P1, this.firstPlayer))
            game.makeMove(new Move(Position.P10, this.secondPlayer))
            game.makeMove(new Move(Position.P11, this.secondPlayer))
            game.makeMove(new Move(Position.P1, this.firstPlayer))
            game.makeMove(new Move(Position.P13, this.secondPlayer))
            game.makeMove(new Move(Position.P1, this.firstPlayer))
            game.makeMove(new Move(Position.P12, this.secondPlayer))
            game.makeMove(new Move(Position.P1, this.firstPlayer))
            return game
        }

        private Game withStones_2InP11_0InP13_26InP14_0InP1() {
            Game game = withStones_4InP8_0InP12_11InP2_4InP14()
            game.makeMove(new Move(Position.P8, this.secondPlayer))
            game.makeMove(new Move(Position.P6, this.firstPlayer))
            game.makeMove(new Move(Position.P13, this.secondPlayer))
            game.makeMove(new Move(Position.P9, this.secondPlayer))
            game.makeMove(new Move(Position.P1, this.firstPlayer))
            game.makeMove(new Move(Position.P13, this.secondPlayer))
            game.makeMove(new Move(Position.P8, this.secondPlayer))
            game.makeMove(new Move(Position.P3, this.firstPlayer))
            return game
        }
    }
}
