package io.powroseba.kalaha.domain

import io.powroseba.kalaha.domain.dto.PitDto
import io.powroseba.kalaha.domain.players.Player
import spock.lang.Specification
import spock.lang.Unroll

class PitTest extends Specification {

    private static final Player TEST_OWNER = Player.first(UUID.randomUUID())

    def 'should return true if pit is empty (do not have any stones)'() {
        given:
            Pit pit = Pit.home(TEST_OWNER)

        when:
            boolean result = pit.isEmpty()

        then:
            result
            noExceptionThrown()
    }

    @Unroll
    def 'should return #expectedResult if pit is owned by specific player #possibleOwner'() {
        given:
            Pit pit = Pit.simple(player)

        when:
            boolean result = pit.isOwnedBy(possibleOwner)

        then:
            result == expectedResult
            noExceptionThrown()

        where:
            player     | possibleOwner                   || expectedResult
            TEST_OWNER | player                          || true
            TEST_OWNER | Player.first(UUID.randomUUID()) || false

    }

    @Unroll
    def 'should return #expectedResult if pit is home pit of specific player = #possibleOwner'() {
        given:
            Pit pit = Pit.home(player)

        when:
            boolean result = pit.isHomeOf(possibleOwner)

        then:
            result == expectedResult
            noExceptionThrown()

        where:
            player     | possibleOwner                    || expectedResult
            TEST_OWNER | player                           || true
            TEST_OWNER | Player.first(UUID.randomUUID())  || false
            TEST_OWNER | Player.second(UUID.randomUUID()) || false
    }

    def 'simple pit should not be a home pit of owner'() {
        given:
            final player = Player.first(UUID.randomUUID())
            Pit pit = Pit.simple(player)
        when:
            boolean result = pit.isHomeOf(player)

        then:
            !result
            noExceptionThrown()
    }

    def 'simple pit should not be home pit and should contain 4 stones'() {
        when:
            Pit pit = Pit.simple(TEST_OWNER)

        then:
            pit == new Pit(4, false, TEST_OWNER)
            noExceptionThrown()
    }

    def 'home pit should be home pit for owner and does not contain any stones'() {
        when:
            Pit pit = Pit.home(TEST_OWNER)

        then:
            pit == new Pit(0, true, TEST_OWNER)
            noExceptionThrown()
    }

    def 'should move single stone from one pit to another'() {
        given:
            Pit source = Pit.simple(TEST_OWNER)
            Pit destination = Pit.simple(TEST_OWNER)

        when:
            source.moveStoneTo(destination)

        then:
            destination.haveMoreStonesThan(source)
            source == new Pit(3, false, TEST_OWNER)
            destination == new Pit(5, false, TEST_OWNER)
            noExceptionThrown()
    }

    def 'should not move stone from empty pit to another'() {
        given:
            Pit emptyPit = Pit.home(TEST_OWNER)
            Pit destination = Pit.simple(TEST_OWNER)

        when:
            emptyPit.moveStoneTo(destination)

        then:
            emptyPit.isEmpty()
            destination == new Pit(4, false, TEST_OWNER)
            noExceptionThrown()
    }

    def 'should move all stones from one pit to another'() {
        given:
            Pit source = Pit.simple(TEST_OWNER)
            Pit destination = Pit.simple(TEST_OWNER)

        when:
            source.moveStonesTo(destination)

        then:
            source.isEmpty()
            destination.haveMoreStonesThan(source)
            destination == new Pit(8, false, TEST_OWNER)
            noExceptionThrown()
    }

    def 'should not move any stone from empty pit to another'() {
        given:
            Pit emptyPit = Pit.home(TEST_OWNER)
            Pit destination = Pit.simple(TEST_OWNER)

        when:
            emptyPit.moveStonesTo(destination)

        then:
            emptyPit.isEmpty()
            destination == new Pit(4, false, TEST_OWNER)
            noExceptionThrown()
    }

    def 'should transform pit to pitDto'() {
        given:
            Pit pit = Pit.simple(TEST_OWNER)

        when:
            PitDto dto = pit.toDto()

        then:
            dto.stonesAmount == 4
            !dto.home
            dto.owner == TEST_OWNER
            noExceptionThrown()
    }

}
