

# Kalaha game https://en.wikipedia.org/wiki/Kalah

### Build and run
To build application you need to have installed maven or use your IDE. 

In case of build and run from terminal please execute following commands from root directory of code base
> ```
> mvn clean package && java -jar kalaha.jar
>```


### How to play ?
In this simple example I've create domain model for kalaha game, still there is plenty thinks to do 
like more user friendly responses but game is ready to use. 

#### Starting the game
* Swagger 
   http://localhost:8080/swagger-ui/index.html#/game-resource/startUsingPOST
* Curl
  ```
  curl -XPOST http://localhost:8080/games -d '{                     
    "firstPlayerId": "dd9805e2-5343-456c-adab-172d5a0c51bd",
    "secondPlayerId": "f7c02f95-b4a6-42f4-a2ba-258fbbd0206e"
  }' -H "Content-Type: application/json"
  
  ```

#### Make move 
gameId - identifier of game returned start game response
pitId - pit identifier where identifiers are : [ P1, P2, P3, P4, P5, P6, P7, P8, P9, P10, P11, P12, P13, P14]
* Swagger http://localhost:8080/swagger-ui/index.html#/game-resource/makeMoveUsingPUT
* Curl 
   ```
   curl -XPUT http://localhost:8080/games/{gameId}/pits/{pitId} -H "Content-Type: application/json"
   ```

#### Display game status
gameId - identifier of game returned start game response

* Swagger http://localhost:8080/swagger-ui/index.html#/game-resource/getGameUsingGET
* Curl 
   ```
   curl -XGET http://localhost:8080/games/{gameId} -H "Content-Type: application/json"
   ```

The methods used in the projects:
1. Hexagonal architecture
2. Test Driven Development

