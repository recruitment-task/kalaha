package io.powroseba.kalaha.web.config;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
@ConditionalOnProperty(prefix = "app.enable", name = "swagger", havingValue = "true")
class SwaggerConfig {

    private static final String BASE_CONTROLLER_PACKAGE = "io.powroseba.kalaha.web.api";

    @Bean
    Docket swaggerDocket() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage(BASE_CONTROLLER_PACKAGE))
                .paths(PathSelectors.any())
                .build();
    }

    @Component
    private static class SwaggerUiWebMvcConfigurer implements WebMvcConfigurer {

        @Override
        public void addResourceHandlers(ResourceHandlerRegistry registry) {
            registry.
                    addResourceHandler("/swagger-ui/**")
                    .addResourceLocations("classpath:/META-INF/resources/webjars/springfox-swagger-ui/")
                    .resourceChain(false);
        }

        @Override
        public void addViewControllers(ViewControllerRegistry registry) {
            registry.addViewController("/swagger")
                    .setViewName("redirect:/swagger-ui/index.html");
        }
    }
}
