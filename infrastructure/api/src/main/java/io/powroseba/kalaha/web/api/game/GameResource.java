package io.powroseba.kalaha.web.api.game;

import io.powroseba.kalaha.application.game.GameExecutor;
import io.powroseba.kalaha.application.game.GameFinder;
import io.powroseba.kalaha.domain.Position;
import io.powroseba.kalaha.domain.dto.GameDto;
import io.powroseba.kalaha.domain.dto.MoveDto;
import io.powroseba.kalaha.web.api.game.dto.GameStateDto;
import io.powroseba.kalaha.web.api.game.dto.StartGameRequest;
import io.powroseba.kalaha.web.api.game.dto.StartGameResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping("/games")
class GameResource {

    private static final Logger logger = LoggerFactory.getLogger(GameResource.class);

    private final GameExecutor gameExecutor;
    private final GameFinder gameFinder;

    GameResource(GameExecutor gameExecutor, GameFinder gameFinder) {
        this.gameExecutor = gameExecutor;
        this.gameFinder = gameFinder;
    }

    @PostMapping
    ResponseEntity<StartGameResponse> start(@RequestBody StartGameRequest request) {
        logger.info("Starting the game ... ");
        GameDto game = gameExecutor.start(request.getFirstPlayer(), request.getSecondPlayer());
        return new ResponseEntity<>(new StartGameResponse(game.gameId), HttpStatus.CREATED);
    }

    @PutMapping("/{gameId}/pits/{pitId}")
    ResponseEntity<GameStateDto> makeMove(@PathVariable UUID gameId, @PathVariable Position pitId) {
        logger.info("Making a move ... ");
        GameDto gameDto = gameExecutor.makeMove(new MoveDto(gameId, pitId));
        return new ResponseEntity<>(new GameStateDto(gameDto.gameId, gameDto.getPits()), HttpStatus.OK);
    }

    @GetMapping("/{gameId}")
    ResponseEntity<GameStateDto> getGame(@PathVariable UUID gameId) {
        logger.info("Getting state of game ... ");
        final GameDto gameDto = gameFinder.findGameById(gameId);
        return new ResponseEntity<>(new GameStateDto(gameDto.gameId, gameDto.getPits()), HttpStatus.OK);
    }

}
