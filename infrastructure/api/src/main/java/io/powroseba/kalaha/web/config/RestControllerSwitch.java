package io.powroseba.kalaha.web.config;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("io.powroseba.kalaha.web.api.**")
@ConditionalOnProperty(prefix = "app.enable", name = "rest-api", havingValue = "true")
class RestControllerSwitch {
}
