package io.powroseba.kalaha.web.api.game.dto;

import java.util.UUID;

public class StartGameResponse {

    private final UUID id;
    private final String url;

    public StartGameResponse(UUID id) {
        this.id = id;
        this.url = "http://localhost:8080/games/" + id;
    }

    public UUID getId() {
        return id;
    }

    public String getUrl() {
        return url;
    }
}
