package io.powroseba.kalaha.web.api.game.dto;

import io.powroseba.kalaha.domain.players.FirstPlayer;
import io.powroseba.kalaha.domain.players.Player;
import io.powroseba.kalaha.domain.players.SecondPlayer;

import java.util.UUID;

public class StartGameRequest {

    private UUID firstPlayerId;
    private UUID secondPlayerId;

    public StartGameRequest() {
    }

    public void setFirstPlayerId(UUID firstPlayerId) {
        this.firstPlayerId = firstPlayerId;
    }

    public void setSecondPlayerId(UUID secondPlayerId) {
        this.secondPlayerId = secondPlayerId;
    }

    public FirstPlayer getFirstPlayer() {
        return Player.first(firstPlayerId);
    }

    public SecondPlayer getSecondPlayer() {
        return Player.second(secondPlayerId);
    }

}
