package io.powroseba.kalaha.web.api.game.dto;

import io.powroseba.kalaha.domain.Position;
import io.powroseba.kalaha.domain.dto.PitDto;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;
import java.util.UUID;

public class GameStateDto {

    private final UUID id;
    private final String url;
    private final Map<Position, Integer> status = new TreeMap<>();

    public GameStateDto(UUID id, Map<Position, PitDto> pits) {
        pits.entrySet()
                .stream()
                .sorted(Map.Entry.comparingByKey())
                .forEachOrdered(entry -> this.status.put(entry.getKey(), entry.getValue().stonesAmount));
        this.id = id;
        this.url = "http://localhost:8080/games/" + id;

    }

    public Map<Position, Integer> getStatus() {
        return status;
    }

    public UUID getId() {
        return id;
    }

    public String getUrl() {
        return url;
    }
}
