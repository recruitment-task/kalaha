package io.powroseba.kalaha.web

import io.powroseba.kalaha.application.game.GameExecutor
import io.powroseba.kalaha.application.game.GameFinder
import io.powroseba.kalaha.domain.Position
import io.powroseba.kalaha.domain.dto.BoardDto
import io.powroseba.kalaha.domain.dto.GameDto
import io.powroseba.kalaha.domain.dto.MoveDto
import io.powroseba.kalaha.domain.dto.PitDto
import io.powroseba.kalaha.domain.players.FirstPlayer
import io.powroseba.kalaha.domain.players.Player
import io.powroseba.kalaha.domain.players.SecondPlayer
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.context.annotation.Bean

@SpringBootApplication
class ApiInitializer {

    private static final UUID TEST_UUID = UUID.fromString("4806bf78-bb15-47ef-a3f4-7410a4114fbc")
    private static final FirstPlayer TURN_PLAYER = Player.first(TEST_UUID)
    private static final GameDto TEST_GAME_DTO = new GameDto(
        TEST_UUID, Player.first(TEST_UUID),
        new BoardDto(
            TURN_PLAYER, Player.second(TEST_UUID), Map.of(Position.P1, new PitDto(1, TURN_PLAYER, false))
        )
    )

    static void main(String[] args) {
        SpringApplication.run(ApiInitializer, args)
    }

    @Bean
    GameExecutor gameExecutor() {
        return new GameExecutor() {

            @Override
            GameDto start(FirstPlayer firstPlayer, SecondPlayer secondPlayer) {
                TEST_GAME_DTO
            }

            @Override
            GameDto makeMove(MoveDto moveDto) {
                TEST_GAME_DTO
            }
        }
    }

    @Bean
    GameFinder gameFinder() {
        return { id -> TEST_GAME_DTO }
    }
}
