package io.powroseba.kalaha.web.api.game

import io.powroseba.kalaha.domain.Position
import io.powroseba.kalaha.web.ApiInitializer
import io.powroseba.kalaha.web.api.game.dto.TestGameStateDto
import io.powroseba.kalaha.web.api.game.dto.TestStartGameRequest
import io.powroseba.kalaha.web.api.game.dto.TestStartGameResponse
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.boot.web.server.LocalServerPort
import org.springframework.http.HttpEntity
import org.springframework.http.HttpMethod
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import spock.lang.Specification

@SpringBootTest(classes = ApiInitializer, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class GameResourceTest extends Specification {

    private static final UUID TEST_UUID = UUID.fromString("4806bf78-bb15-47ef-a3f4-7410a4114fbc")

    @LocalServerPort
    private int port

    @Autowired
    private TestRestTemplate restTemplate

    def 'should return game information'() {
        given:
            final String url = "http://localhost:" + port + "/games/" + UUID.randomUUID().toString()

        when:
            ResponseEntity<TestGameStateDto> game = restTemplate.getForEntity(url, TestGameStateDto)

        then:
            game.getStatusCode() == HttpStatus.OK
            with(game.getBody()) {
                getId() == TEST_UUID
                getStatus().get(Position.P1) == 1
                getUrl() == "http://localhost:8080/games/" + TEST_UUID
            }
            noExceptionThrown()
    }

    def 'should start game and return game informations'() {
        given:
            final String url = "http://localhost:" + port + "/games"
            final TestStartGameRequest request = new TestStartGameRequest(TEST_UUID.toString(), TEST_UUID.toString())

        when:
            ResponseEntity<TestStartGameResponse> game = restTemplate.exchange(url, HttpMethod.POST, new HttpEntity<TestStartGameRequest>(request), TestStartGameResponse)

        then:
            game.getStatusCode() == HttpStatus.CREATED
            with(game.getBody()) {
                getId() == TEST_UUID
                getUrl() == "http://localhost:8080/games/" + TEST_UUID
            }
            noExceptionThrown()
    }

    def 'should make move in game and return game state'() {
        given:
            final String url = "http://localhost:" + port + "/games/" + TEST_UUID + "/pits/" + Position.P1

        when:
            ResponseEntity<TestGameStateDto> game = restTemplate.exchange(url, HttpMethod.PUT, null, TestGameStateDto)

        then:
            game.getStatusCode() == HttpStatus.OK
            with(game.getBody()) {
                getId() == TEST_UUID
                getUrl() == "http://localhost:8080/games/" + TEST_UUID
            }
            noExceptionThrown()
    }
}
