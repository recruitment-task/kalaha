package io.powroseba.kalaha.web.api.game.dto

class TestStartGameResponse {

    private UUID id
    private String url

    TestStartGameResponse() {
    }

    UUID getId() {
        return id
    }

    void setId(UUID id) {
        this.id = id
    }

    String getUrl() {
        return url
    }

    void setUrl(String url) {
        this.url = url
    }
}
