package io.powroseba.kalaha.web.api.game.dto

class TestStartGameRequest {
    private final String firstPlayerId
    private final String secondPlayerId

    TestStartGameRequest(String firstPlayerId, String secondPlayerId) {
        this.firstPlayerId = firstPlayerId
        this.secondPlayerId = secondPlayerId
    }

    String getFirstPlayerId() {
        return firstPlayerId
    }

    String getSecondPlayerId() {
        return secondPlayerId
    }
}
