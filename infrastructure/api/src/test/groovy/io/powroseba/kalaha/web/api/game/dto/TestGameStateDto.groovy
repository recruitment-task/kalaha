package io.powroseba.kalaha.web.api.game.dto

import io.powroseba.kalaha.domain.Position

class TestGameStateDto {
    private UUID id
    private String url
    private Map<Position, Integer> status = new HashMap<>()

    TestGameStateDto() {}

    UUID getId() {
        return id
    }

    void setId(UUID id) {
        this.id = id
    }

    String getUrl() {
        return url
    }

    void setUrl(String url) {
        this.url = url
    }

    Map<Position, Integer> getStatus() {
        return status
    }

    void setStatus(Map<Position, Integer> status) {
        this.status = status
    }
}
