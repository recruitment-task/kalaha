package io.powroseba.kalaha.persistence.entity;

import io.powroseba.kalaha.domain.Position;
import io.powroseba.kalaha.domain.dto.BoardDto;
import io.powroseba.kalaha.domain.dto.GameDto;
import io.powroseba.kalaha.domain.dto.PitDto;
import io.powroseba.kalaha.domain.players.FirstPlayer;
import io.powroseba.kalaha.domain.players.Player;
import io.powroseba.kalaha.domain.players.SecondPlayer;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class GameEntity {
    private final UUID id;
    private final Player turn;
    private final FirstPlayer firstPlayer;
    private final SecondPlayer secondPlayer;
    private final PitDto position1Pit;
    private final PitDto position2Pit;
    private final PitDto position3Pit;
    private final PitDto position4Pit;
    private final PitDto position5Pit;
    private final PitDto position6Pit;
    private final PitDto position7Pit;
    private final PitDto position8Pit;
    private final PitDto position9Pit;
    private final PitDto position10Pit;
    private final PitDto position11Pit;
    private final PitDto position12Pit;
    private final PitDto position13Pit;
    private final PitDto position14Pit;

    public GameEntity(UUID id, GameDto game) {
        this.id = id;
        this.turn = game.turn;
        this.firstPlayer = game.getFirstPlayer();
        this.secondPlayer = game.getSecondPlayer();
        this.position1Pit = game.getPits().get(Position.P1);
        this.position2Pit = game.getPits().get(Position.P2);
        this.position3Pit = game.getPits().get(Position.P3);
        this.position4Pit = game.getPits().get(Position.P4);
        this.position5Pit = game.getPits().get(Position.P5);
        this.position6Pit = game.getPits().get(Position.P6);
        this.position7Pit = game.getPits().get(Position.P7);
        this.position8Pit = game.getPits().get(Position.P8);
        this.position9Pit = game.getPits().get(Position.P9);
        this.position10Pit = game.getPits().get(Position.P10);
        this.position11Pit = game.getPits().get(Position.P11);
        this.position12Pit = game.getPits().get(Position.P12);
        this.position13Pit = game.getPits().get(Position.P13);
        this.position14Pit = game.getPits().get(Position.P14);
    }

    public GameDto toDto() {
        Map<Position, PitDto> pits = new HashMap<>();
        pits.put(Position.P1, position1Pit);
        pits.put(Position.P2, position2Pit);
        pits.put(Position.P3, position3Pit);
        pits.put(Position.P4, position4Pit);
        pits.put(Position.P5, position5Pit);
        pits.put(Position.P6, position6Pit);
        pits.put(Position.P7, position7Pit);
        pits.put(Position.P8, position8Pit);
        pits.put(Position.P9, position9Pit);
        pits.put(Position.P10, position10Pit);
        pits.put(Position.P11, position11Pit);
        pits.put(Position.P12, position12Pit);
        pits.put(Position.P13, position13Pit);
        pits.put(Position.P14, position14Pit);
        return new GameDto(this.id, this.turn, new BoardDto(firstPlayer, secondPlayer, pits));
    }


}