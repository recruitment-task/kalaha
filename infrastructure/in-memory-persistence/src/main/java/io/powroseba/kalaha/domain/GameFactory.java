package io.powroseba.kalaha.domain;

import io.powroseba.kalaha.domain.dto.GameDto;
import io.powroseba.kalaha.domain.dto.PitDto;

import java.util.Map;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class GameFactory {

    public Game build(GameDto game) {
        return new Game(
                game.gameId,
                game.getFirstPlayer(),
                game.getSecondPlayer(),
                game.turn,
                getBoardFrom(game)
        );
    }

    private Board getBoardFrom(GameDto game) {
        return new Board(
                game.getFirstPlayer(),
                game.getSecondPlayer(),
                getPitsFrom(game)
        );
    }

    private Map<Position, Pit> getPitsFrom(GameDto game) {
        return game.boardDto.pits
                .entrySet()
                .stream()
                .map(Pair::wrap)
                .collect(Pair.toMap());
    }

    private static class Pair {
        private final Position position;
        private final Pit pit;

        private Pair(Position position, Pit pit) {
            this.position = position;
            this.pit = pit;
        }

        private static Pair wrap(Map.Entry<Position, PitDto> entry) {
            return new Pair(
                    entry.getKey(),
                    new Pit(
                            entry.getValue().stonesAmount,
                            entry.getValue().home,
                            entry.getValue().owner
                    )
            );
        }

        private static Collector<Pair, ?, Map<Position, Pit>> toMap() {
            return Collectors.toMap(p -> p.position, p -> p.pit);
        }
    }
}
