package io.powroseba.kalaha.persistence;

import io.powroseba.kalaha.application.game.GameRepository;
import io.powroseba.kalaha.domain.players.Player;
import io.powroseba.kalaha.application.player.PlayerProvider;
import io.powroseba.kalaha.infrastructure.Adapter;

import java.util.Optional;
import java.util.UUID;

@Adapter
class PlayerProviderAdapter implements PlayerProvider {

    // to always return player who have a turn in game
    private final GameRepository gameRepository;

    PlayerProviderAdapter(GameRepository gameRepository) {
        this.gameRepository = gameRepository;
    }

    @Override
    public Optional<Player> getActionPlayer(UUID gameId) {
        return gameRepository.get(gameId).map(game -> game.getData().turn);
    }

    @Override
    public Optional<? extends Player> getPlayer(Player player) {
        return Optional.of(player);
    }
}
