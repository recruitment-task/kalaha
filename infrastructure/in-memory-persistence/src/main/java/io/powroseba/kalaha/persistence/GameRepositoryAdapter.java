package io.powroseba.kalaha.persistence;

import io.powroseba.kalaha.domain.Game;
import io.powroseba.kalaha.domain.GameFactory;
import io.powroseba.kalaha.application.game.GameRepository;
import io.powroseba.kalaha.domain.dto.GameDto;
import io.powroseba.kalaha.infrastructure.Adapter;
import io.powroseba.kalaha.persistence.entity.GameEntity;

import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

@Adapter
class GameRepositoryAdapter implements GameRepository {

    private static final Map<UUID, GameEntity> games = new ConcurrentHashMap<>();
    private static final GameFactory gameFactory = new GameFactory();

    @Override
    public Optional<Game> get(UUID id) {
        return Optional.ofNullable(gameFactory.build(games.get(id).toDto()));
    }

    @Override
    public GameDto save(GameDto game) {
        final UUID id = game.gameId == null ? UUID.randomUUID() : game.gameId;
        final GameEntity gameEntity = new GameEntity(id, game);
        games.put(id, gameEntity);
        return gameEntity.toDto();
    }
}
