package io.powroseba.kalaha.application.game;

import io.powroseba.kalaha.application.ApplicationService;
import io.powroseba.kalaha.application.player.PlayerProvider;
import io.powroseba.kalaha.domain.Game;
import io.powroseba.kalaha.domain.Move;
import io.powroseba.kalaha.domain.dto.GameDto;
import io.powroseba.kalaha.domain.dto.MoveDto;
import io.powroseba.kalaha.domain.players.FirstPlayer;
import io.powroseba.kalaha.domain.players.Player;
import io.powroseba.kalaha.domain.players.SecondPlayer;

import java.util.UUID;

@ApplicationService
class GameService implements GameFinder, GameExecutor {

    private final GameRepository gameRepository;
    private final PlayerProvider playerProvider;

    public GameService(GameRepository gameRepository, PlayerProvider playerProvider) {
        this.gameRepository = gameRepository;
        this.playerProvider = playerProvider;
    }

    public GameDto start(FirstPlayer firstPlayer, SecondPlayer secondPlayer) {
        validatePlayerExistence(firstPlayer, secondPlayer);
        Game game = Game.start(firstPlayer, secondPlayer);
        return gameRepository.save(game.getData());
    }

    private void validatePlayerExistence(FirstPlayer firstPlayer, SecondPlayer secondPlayer) {
        playerProvider.getPlayer(firstPlayer).orElseThrow(IllegalArgumentException::new);
        playerProvider.getPlayer(secondPlayer).orElseThrow(IllegalArgumentException::new);
    }

    public GameDto makeMove(MoveDto moveDto) {
        Player actionPlayer = playerProvider.getActionPlayer(moveDto.gameId).orElseThrow(IllegalArgumentException::new);
        Game game = gameRepository.get(moveDto.gameId).orElseThrow(IllegalArgumentException::new);
        game.makeMove(new Move(moveDto.position, actionPlayer));
        return gameRepository.save(game.getData());
    }

    public GameDto findGameById(UUID gameId) {
        return gameRepository.get(gameId).map(Game::getData).orElseThrow(IllegalArgumentException::new);
    }
}
