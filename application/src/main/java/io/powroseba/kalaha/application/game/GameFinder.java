package io.powroseba.kalaha.application.game;

import io.powroseba.kalaha.domain.dto.GameDto;

import java.util.UUID;

public interface GameFinder {

    GameDto findGameById(UUID id);
}
