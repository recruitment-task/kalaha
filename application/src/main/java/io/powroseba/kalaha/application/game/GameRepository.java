package io.powroseba.kalaha.application.game;

import io.powroseba.kalaha.domain.Game;
import io.powroseba.kalaha.domain.dto.GameDto;

import java.util.Optional;
import java.util.UUID;

public interface GameRepository {

    Optional<Game> get(UUID id);

    GameDto save(GameDto game);
}
