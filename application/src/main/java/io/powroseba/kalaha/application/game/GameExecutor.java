package io.powroseba.kalaha.application.game;

import io.powroseba.kalaha.domain.dto.GameDto;
import io.powroseba.kalaha.domain.dto.MoveDto;
import io.powroseba.kalaha.domain.players.FirstPlayer;
import io.powroseba.kalaha.domain.players.SecondPlayer;

public interface GameExecutor {

    GameDto start(FirstPlayer firstPlayer, SecondPlayer secondPlayer);

    GameDto makeMove(MoveDto moveDto);
}
