package io.powroseba.kalaha.application.player;

import io.powroseba.kalaha.domain.players.Player;

import java.util.Optional;
import java.util.UUID;

public interface PlayerProvider {

    Optional<Player> getActionPlayer(UUID gameId);

    Optional<? extends Player> getPlayer(Player player);
}
