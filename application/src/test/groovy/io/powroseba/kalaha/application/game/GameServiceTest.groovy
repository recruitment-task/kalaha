package io.powroseba.kalaha.application.game


import io.powroseba.kalaha.domain.*
import io.powroseba.kalaha.domain.dto.BoardDto
import io.powroseba.kalaha.domain.dto.GameDto
import io.powroseba.kalaha.domain.dto.MoveDto
import io.powroseba.kalaha.domain.dto.PitDto
import io.powroseba.kalaha.domain.players.FirstPlayer
import io.powroseba.kalaha.domain.players.Player
import io.powroseba.kalaha.application.player.PlayerProvider
import io.powroseba.kalaha.domain.players.SecondPlayer
import spock.lang.Specification
import spock.lang.Unroll

class GameServiceTest extends Specification {

    private final static FirstPlayer FIRST_PLAYER = Player.first(UUID.randomUUID())
    private final static SecondPlayer SECOND_PLAYER = Player.second(UUID.randomUUID())
    private final static BoardDto INITIAL_PITS = new BoardDataFactory(FIRST_PLAYER, SECOND_PLAYER).initialData()

    private GameRepository gameRepository = Mock(GameRepository)
    private PlayerProvider playerProvider = Mock(PlayerProvider)
    private GameService facade = new GameService(gameRepository, playerProvider)

    @Unroll
    def 'should not start game when player has not been found'() {
        when:
            facade.start(FIRST_PLAYER, SECOND_PLAYER)

        then:
            1 * playerProvider.getPlayer(_) >> Optional.empty()
            0 * gameRepository.save(_)
            thrown(IllegalArgumentException)
    }

    def 'should start game and return game dto with correct data'() {
        given:
            GameDto game = Game.start(FIRST_PLAYER, SECOND_PLAYER).getData()

        when:
            GameDto dto = facade.start(FIRST_PLAYER, SECOND_PLAYER)

        then:
            1 * playerProvider.getPlayer(FIRST_PLAYER) >> Optional.ofNullable(FIRST_PLAYER)
            1 * playerProvider.getPlayer(SECOND_PLAYER) >> Optional.ofNullable(SECOND_PLAYER)
            1 * gameRepository.save({  gameDto -> validateGameDto(gameDto) }) >> game
            validateGameDto(dto)
            noExceptionThrown()
    }

    def 'should throw IllegalArgumentException when player which try to make move does not exist'() {
        when:
            facade.makeMove(new MoveDto(UUID.randomUUID(), Position.P1))
        then:
            1 * playerProvider.getActionPlayer(_) >> Optional.empty()
            0 * gameRepository.get(_)
            0 * gameRepository.save(_)
            thrown(IllegalArgumentException)
    }

    def 'should throw IllegalArgumentException when game does not exist'() {
        given:
            MoveDto move = new MoveDto(UUID.randomUUID(), Position.P1)

        when:
            facade.makeMove(move)
        then:
            1 * playerProvider.getActionPlayer(_) >> Optional.of(FIRST_PLAYER)
            1 * gameRepository.get(move.gameId) >> Optional.empty()
            0 * gameRepository.save(_)
            thrown(IllegalArgumentException)
    }

    def 'should properly execute move and return game data'() {
        given:
            final UUID gameId = UUID.randomUUID()
            MoveDto move = new MoveDto(gameId, Position.P1)
            Game game = Game.start(FIRST_PLAYER, SECOND_PLAYER)

        when:
            GameDto gameDto = facade.makeMove(move)
        then:
            1 * playerProvider.getActionPlayer(_) >> Optional.of(FIRST_PLAYER)
            1 * gameRepository.get(move.gameId) >> Optional.of(game)
            1 * gameRepository.save({ dto -> validateGameDtoAfter1Step(dto)}) >> game.getData()
            validateGameDto(gameDto)
            noExceptionThrown()
    }

    void validateGameDto(GameDto dto) {
        with (dto) {
            getFirstPlayer() == FIRST_PLAYER
            getSecondPlayer() == SECOND_PLAYER
            turn == FIRST_PLAYER
            for (Position position : Position.values()) {
                comparePits(getPits().get(position), INITIAL_PITS.pits.get(position))
            }
        }
    }

    void validateGameDtoAfter1Step(GameDto dto) {
        with (dto) {
            getFirstPlayer() == FIRST_PLAYER
            getSecondPlayer() == SECOND_PLAYER
            turn == SECOND_PLAYER
            for (Position position : Position.values()) {
                if (Arrays.asList(Position.P1, Position.P2, Position.P3, Position.P4, Position.P5).contains(position)) {
                    if (Position.P1 == position) {
                        getPits().get(position).stonesAmount == 0
                    } else {
                        getPits().get(position).stonesAmount == 5
                    }
                    getPits().get(position).home == INITIAL_PITS.pits.get(position).home
                    getPits().get(position).owner == INITIAL_PITS.pits.get(position).owner
                } else {
                    comparePits(getPits().get(position), INITIAL_PITS.pits.get(position))
                }
            }
        }
    }

    private static boolean comparePits(PitDto pit, PitDto otherPit) {
        return pit.home == otherPit.home && pit.owner == otherPit.owner && pit.stonesAmount == otherPit.stonesAmount
    }
}
