package io.powroseba.kalaha.application.game


import io.powroseba.kalaha.domain.Position
import io.powroseba.kalaha.domain.dto.BoardDto
import io.powroseba.kalaha.domain.dto.PitDto
import io.powroseba.kalaha.domain.players.FirstPlayer
import io.powroseba.kalaha.domain.players.SecondPlayer

import static io.powroseba.kalaha.domain.Position.*

class BoardDataFactory {

    private final FirstPlayer firstPlayer
    private final SecondPlayer secondPlayer

    BoardDataFactory(FirstPlayer firstPlayer, SecondPlayer secondPlayer) {
        this.firstPlayer = firstPlayer
        this.secondPlayer = secondPlayer
    }

    BoardDto initialData() {
        return new BoardDto(firstPlayer, secondPlayer, initialPits())
    }

    private HashMap<Position, PitDto> initialPits() {
        Map<Position, PitDto> pits = new HashMap<>()
        pits.put(P1, new PitDto(4, firstPlayer, false))
        pits.put(P2, new PitDto(4, firstPlayer, false))
        pits.put(P3, new PitDto(4, firstPlayer, false))
        pits.put(P4, new PitDto(4, firstPlayer, false))
        pits.put(P5, new PitDto(4, firstPlayer, false))
        pits.put(P6, new PitDto(4, firstPlayer, false))
        pits.put(P7, new PitDto(0, firstPlayer, true))
        pits.put(P8, new PitDto(4, secondPlayer, false))
        pits.put(P9, new PitDto(4, secondPlayer, false))
        pits.put(P10, new PitDto(4, secondPlayer, false))
        pits.put(P11, new PitDto(4, secondPlayer, false))
        pits.put(P12, new PitDto(4, secondPlayer, false))
        pits.put(P13, new PitDto(4, secondPlayer, false))
        pits.put(P14, new PitDto(0, secondPlayer, true))
        pits
    }

}
