package io.powroseba.kalaha.starter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(
        basePackages = { "io.powroseba.kalaha.starter.**", "**.config.**"}
)
public class KalahaInitializer {

    public static void main(String[] args) {
        SpringApplication.run(KalahaInitializer.class, args);
    }
}
