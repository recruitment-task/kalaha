package io.powroseba.kalaha.starter.config;


import io.powroseba.kalaha.infrastructure.Adapter;
import org.springframework.stereotype.Component;

@Component
class InfrastructureConfig extends AbstractAnnotationBeanRegistry {

    InfrastructureConfig() {
        super(Adapter.class, "io.powroseba.kalaha.persistence");
    }
}
