package io.powroseba.kalaha.starter.config;

import io.powroseba.kalaha.application.ApplicationService;
import org.springframework.stereotype.Component;

@Component
class ApplicationServiceConfig extends AbstractAnnotationBeanRegistry {

    ApplicationServiceConfig() {
        super(ApplicationService.class, "io.powroseba.kalaha.application");
    }
}
