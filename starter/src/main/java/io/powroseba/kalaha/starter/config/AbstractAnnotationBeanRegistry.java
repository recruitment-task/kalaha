package io.powroseba.kalaha.starter.config;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.BeanDefinitionRegistryPostProcessor;
import org.springframework.context.annotation.ClassPathScanningCandidateComponentProvider;
import org.springframework.core.type.filter.AnnotationTypeFilter;

import java.lang.annotation.Annotation;

abstract class AbstractAnnotationBeanRegistry implements BeanDefinitionRegistryPostProcessor {

    private final Class<? extends Annotation> annotationType;
    private final String basePackage;

    AbstractAnnotationBeanRegistry(Class<? extends Annotation> annotationType, String basePackage) {
        this.annotationType = annotationType;
        this.basePackage = basePackage;
    }

    @Override
    public void postProcessBeanDefinitionRegistry(BeanDefinitionRegistry beanDefinitionRegistry) throws BeansException {
        ClassPathScanningCandidateComponentProvider scanner = new ClassPathScanningCandidateComponentProvider(false);
        scanner.addIncludeFilter(new AnnotationTypeFilter(annotationType));
        scanner.findCandidateComponents(basePackage)
                .stream()
                .map(BeanCandidate::new)
                .filter(bean -> beanDoesNotExist(beanDefinitionRegistry, bean))
                .forEach(bean -> registerBean(beanDefinitionRegistry, bean));

    }

    private boolean beanDoesNotExist(BeanDefinitionRegistry beanDefinitionRegistry, BeanCandidate bean) {
        return !beanDefinitionRegistry.containsBeanDefinition(bean.beanName);
    }

    private void registerBean(BeanDefinitionRegistry beanDefinitionRegistry, BeanCandidate bean) {
        beanDefinitionRegistry.registerBeanDefinition(bean.beanName, bean.beanDefinition);
    }

    @Override
    public void postProcessBeanFactory(ConfigurableListableBeanFactory configurableListableBeanFactory) throws BeansException {}

    private static final class BeanCandidate {
        private final String beanName;
        private final BeanDefinition beanDefinition;

        BeanCandidate(BeanDefinition beanDefinition) {
            if (beanDefinition.getBeanClassName() == null) {
                throw new IllegalArgumentException("Bean does not have required name!");
            }
            this.beanDefinition = beanDefinition;
            this.beanName = getBeanName(beanDefinition);
        }

        private String getBeanName(BeanDefinition definition) {
            return definition.getBeanClassName().substring(definition.getBeanClassName().lastIndexOf(".") + 1);
        }
    }
}
